module gitlab.com/golight/migrator

go 1.19

require (
	github.com/Masterminds/squirrel v1.5.4
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.2.0
	github.com/valyala/quicktemplate v1.7.0
	gitlab.com/golight/dao v1.0.8
	gitlab.com/golight/scanner v1.0.6
	golang.org/x/sync v0.5.0
)

require (
	github.com/lann/builder v0.0.0-20180802200727-47ae307949d0 // indirect
	github.com/lann/ps v0.0.0-20150810152359-62de8c46ede0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	gitlab.com/golight/entity v1.0.0 // indirect
)
